# Deploying Grafana and Prometheus using Helm

This README provides a concise guide on how to deploy Grafana and Prometheus using Helm charts. Helm is a package manager for Kubernetes applications, and it simplifies the process of deploying, managing, and scaling applications on Kubernetes clusters.

## Prerequisites

1. Kubernetes Cluster: Ensure you have a functional Kubernetes cluster to deploy Grafana and Prometheus.

2. Helm Installed: Make sure you have Helm installed on your local machine or the machine from which you'll be deploying.

3. Helm Repository: Add the necessary Helm repositories for Grafana and Prometheus charts. You can use the official Helm Hub repositories or other trusted sources.

## Steps to Deploy

### 1. Add Helm Repositories

```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```

### 2. Deploy Prometheus

```bash
helm upgrade --install prometheus prometheus-community/prometheus -f prom-values.yaml --namespace observation
```

### 3. Deploy Grafana

```bash
helm upgrade --install grafana grafana/grafana -f grafana-values.yaml --namespace observation
```
### 4. Add Data Source:
```bash
Prometheus: url- http://prometheus-server.observation.svc.cluster.local
postgres: url - postgresdb.database.svc.cluster.local
``` 


### 4. Clean Up (Optional)

To uninstall the deployed charts:

```bash
helm uninstall prometheus
helm uninstall grafana
```
